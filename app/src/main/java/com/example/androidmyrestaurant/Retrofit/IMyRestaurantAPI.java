package com.example.androidmyrestaurant.Retrofit;

import com.example.androidmyrestaurant.Model.AddonModel;
import com.example.androidmyrestaurant.Model.MenuModel;
import com.example.androidmyrestaurant.Model.RestaurantModel;
import com.example.androidmyrestaurant.Model.SizeModel;
import com.example.androidmyrestaurant.Model.UpdateUserModel;
import com.example.androidmyrestaurant.Model.UserModel;
import com.example.androidmyrestaurant.Model.FoodModel;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface IMyRestaurantAPI {


    @GET("user")
    Observable <UserModel> getUser(@Query("key") String apikey,
                                   @Query("fbid") String fbid);


    @GET("restaurant")
    Observable <RestaurantModel> getRestaurant (@Query("key") String apikey);


    @GET("menu")
    Observable<MenuModel> getCategories(@Query("key") String apiKey,
                                        @Query("restaurantId") int restaurantId);


    @GET("food")
    Observable<FoodModel> getFoodOfMenu(@Query("key") String apikey,
                                        @Query("menuId") int menuId);


    @GET("searchFood")
    Observable<FoodModel> searchFood(@Query("key") String apikey,
                                     @Query("foodName") String foodName,
                                     @Query("menuId") int menuId);


    @GET("size")
    Observable<SizeModel> getSizeOfFood(@Query("key") String apikey,
                                        @Query("foodId") int foodId);


    @GET("addon")
    Observable<AddonModel> getAddonOfFood(@Query("key") String apikey,
                                          @Query("foodId") int foodId);






    ////////////// POST

    @POST("user")
    @FormUrlEncoded
    Observable<UpdateUserModel> updateUserInfo(@Field("key") String apikey,
                                               @Field("userPhone") String userPhone,
                                               @Field("userName") String userName,
                                               @Field("userAddress") String userAddress,
                                               @Field("fbid") String fbid);

}
